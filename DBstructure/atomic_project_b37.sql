-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2016 at 08:32 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b37`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `birthday` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`, `is_delete`) VALUES
(1, 'Sajeda Yeasmin', '05/11/1993', 1),
(4, 'ya ', '1993-09-08 ', 1),
(8, 'Nilufar Jahan', '0134-03-02', 1),
(10, 'IIUc', '0007-05-06', 1),
(11, 'Nilufar Jahan ', '0021-02-13', 0),
(12, 'Ayath', '1999-09-09', 1),
(13, 'Shajib', '1324-09-05', 0),
(14, 'ABC', '1993-09-08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(13) NOT NULL,
  `book_title` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_delete`) VALUES
(29, 'keya', 'keya', 1),
(30, 'Ayath', 'Ayath', 1),
(31, 'Artifacial Intelligence ', 'Sahadat Hossain ', 1),
(32, 'Sajib  ', 'Skeya ', 0),
(33, 'Shaifur', 'Shaifur', 0),
(34, 'Irean', 'Irean', 0),
(35, 'Nilufar', 'Nilufar', 0),
(36, 'Ayath', 'Ayath', 0),
(37, 'keya', 'keya', 0),
(38, 'Sahadat', 'Sahadat Hossain ', 0),
(39, 'Sobuj', 'Sobuj', 0),
(40, 'PHP', 'QQQ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `country`, `city`, `is_delete`) VALUES
(1, 'Nilufar Jahan', '', '', 0),
(2, 'Nilufar Jahan', 'Albania', 'Berat', 0),
(3, 'Nilufar Jahan ', 'Bahamas', 'Nicholls Town and Berry Islands', 0),
(4, 'ABC', 'Bangladesh', 'Dhaka', 0),
(5, 'Nilufar Jahan', 'Barbados', 'Bridgetown', 0),
(6, 'AB ', 'Azerbaijan', 'Davaci Rayonu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `is_delete`) VALUES
(1, 'Sajeda Yeasmin', 'sajyeasmin@gmail.com', 0),
(2, 'Nilufar', 'nilufareprea@gmail.com', 0),
(4, 'Nilufar', 'nilufareprea@gmail.com', 0),
(5, 'rrrrrrr', 'rrrrr@gmail.com', 0),
(6, 'Nilufar Jahanrrr', 'nilufareprea@gmail.com', 0),
(7, 'ABC', 'abc@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `is_delete`) VALUES
(2, 'Sajeda Yeasmin     ', 'Other', 1),
(6, 'keya  ', 'Male', 1),
(7, 'Nilufar Jahan', 'male', 1),
(8, 'Nilufar Jahan', 'female', 0),
(9, 'Nilufar Jahan', 'female', 0),
(10, 'Nilufar Jahanrrr', 'female', 0),
(11, 'Nilufar', 'female', 0),
(12, 'Nilufar', 'male', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `hobbies` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `is_delete`) VALUES
(11, 'Nilufar Jahan  ', 'Reading,Writing,Drawing', 1),
(12, 'keya  ', 'Programming,Reading,Writing,Drawing', 0),
(13, 'Nilufar ', 'Programming,Reading,Writing,Drawing', 0),
(14, 'ABC ', 'Reading,Writing', 0);

-- --------------------------------------------------------

--
-- Table structure for table `organization_of_summary`
--

CREATE TABLE `organization_of_summary` (
  `id` int(50) NOT NULL,
  `org_name` varchar(200) NOT NULL,
  `org_summary` text NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organization_of_summary`
--

INSERT INTO `organization_of_summary` (`id`, `org_name`, `org_summary`, `is_delete`) VALUES
(1, 'Grammenphone', 'Grameenphone widely abbreviated as GP, is the leading telecommunications service provider in Bangladesh. With more than 56 million subscribers (as of January 2016),Grameenphone is the largest mobile phone operator in the country.', 1),
(5, 'Weedvffdgv', 'sdfgnhjmm\r\n', 1),
(6, 'keya', 'jhgfgd\r\nzdfvgbnhjgmk', 1),
(8, 'Intel', 'hardawre\r\n', 1),
(9, 'Nilufar jahan', 'student\r\n\r\n', 1),
(11, 'ABC', 'sdfb', 1),
(12, 'Dasfzs', 'vdfc', 1),
(13, 'asdf', 'xcsv', 1),
(15, 'xzcvb', 'xcxvbn', 0);

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `profile_picture` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_picture`, `is_delete`) VALUES
(4, 'Shajib', '147949595012642600_1116353551728664_7023052198127649750_n.jpg', 1),
(5, 'Shajib', '147949624110353188_927815540582467_3140302009590628874_n.jpg', 1),
(6, 'shaifur', '147949725610353188_927815540582467_3140302009590628874_n.jpg', 1),
(7, 'Nilufar Jahan', '148016863515094256_1243756318981212_9166033124978018986_n.jpg', 1),
(8, 'Nilufar Jahan', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_of_summary`
--
ALTER TABLE `organization_of_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `organization_of_summary`
--
ALTER TABLE `organization_of_summary`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
