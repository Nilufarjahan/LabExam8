<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Atomic Project</title>

    <link href="resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="resource/bootstrap/css/datepicker3.css" rel="stylesheet">
    <link href="resource/bootstrap/css/styles.css" rel="stylesheet">
    <link rel="stylesheet" href="resource/bootstrap/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="resource/bootstrap/css/font-icons/font-awesome/css/font-awesome.min.css">
    <!--Icons-->
    <script src="resource/bootstrap/js/lumino.glyphs.js"></script>

    <!--[if lt IE 9]>
    <script src="resource/bootstrap/js/html5shiv.min.js"></script>
    <script src="resource/bootstrap/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>Atomic</span>Project</a>
           >
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

    <ul class="nav menu">
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-book"></i>
                <span class="title">Book Title</span>
            </a>
            <ul>
                <li>
                    <a href="views/SEIP137417/BookTitle/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="views/SEIP137417/BookTitle/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="has-sub">
            <a href="#">
                <i class="fa fa-birthday-cake"></i>
                <span class="title">Birth Day</span>
            </a>
            <ul>
                <li>
                    <a href="views/SEIP137417/Birthday/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="views/SEIP137417/Birthday/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-envelope"></i>
                <span class="title">Email Addess</span>
            </a>
            <ul>
                <li>
                    <a href="views/SEIP137417/Email/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="views/SEIP137417/Email/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-male"></i>
                <span class="title">Gender</span>
            </a>
            <ul>
                <li>
                    <a href="views/SEIP137417/Gender/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="views/SEIP137417/Gender/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-camera"></i>
                <span class="title">Profile Picture</span>
            </a>
            <ul>
                <li>
                    <a href="views/SEIP137417/ProfilePicture/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="views/SEIP137417/ProfilePicture/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-tree"></i>
                <span class="title">City Name</span>
            </a>
            <ul>
                <li>
                    <a href="views/SEIP137417/City/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="views/SEIP137417/City/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-tree"></i>
                <span class="title">Hobbies</span>
            </a>
            <ul>
                <li>
                    <a href="views/SEIP137417/Hobbies/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="views/SEIP137417/Hobbies/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-tree"></i>
                <span class="title">Summary Of Organization</span>
            </a>
            <ul>
                <li>
                    <a href="views/SEIP137417/SummaryOfOrganization/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="views/SEIP137417/SummaryOfOrganization/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        </ul>

</div>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li class="active">Icons</li>
        </ol>
    </div><!
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Welcome To My Atomic Project</h1>
    </div>
</div><!--/.row-->

</div>
<script src="resource/bootstrap/js/jquery-1.11.1.min.js"></script>
<script src="resource/bootstrap/js/bootstrap.min.js"></script>
<script src="resource/bootstrap/js/chart.min.js"></script>
<script src="resource/bootstrap/js/chart-data.js"></script>
<script src="resource/bootstrap/js/easypiechart.js"></script>
<script src="resource/bootstrap/js/easypiechart-data.js"></script>
<script src="resource/bootstrap/js/bootstrap-datepicker.js"></script>
<script>
    $('#calendar').datepicker({
    });

    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })
</script>

</body>

</html>
