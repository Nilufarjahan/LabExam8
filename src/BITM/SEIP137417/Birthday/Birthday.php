<?php
namespace App\Birthday;
use App\Model\database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class Birthday extends DB
{
    public $id="";
    public $name="";
    public $birthday="";

    public function __construct(){

        parent::__construct();
    }
    public function setData($data=NULL){
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];

        }

        if (array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }

        if (array_key_exists('birthday',$data))
        {
            $this->birthday=$data['birthday'];
        }


    }
    public function store(){

        $arrData  = array($this->name,$this->birthday);

        $sql = "INSERT INTO birthday ( name, birthday) VALUES ( ?, ?)";

        $STH = $this->DBH->prepare($sql);


        $result=$STH->execute($arrData);
        var_dump($result);
        if($result)
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] , [ Birthday: $this->birthday ] <br> Data Has Been Inserted Successfully!</h5></div>");
        else
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] , [ Birthday: $this->birthday ] <br> Data Has not Been Inserted Successfully!</h5></div>");
        Utility::redirect('create.php');

    }
    public function index($mode="ASSOC"){
        $mode=strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from birthday WHERE is_delete=0');


        if($mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }

    public function view($fetchMode="ASSOC"){

        $STH = $this->DBH->query('SELECT * from birthday where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of view()
    public function update(){

        $arrData  = array($this->name,$this->birthday);

        $sql = 'UPDATE birthday SET    name= ?   , birthday = ? where id ='.$this->id;
        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('index.php');


    }// end of update()
    public function delete(){
        $sql='DELETE FROM birthday WHERE id = '.$this->id;
        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");

        Utility::redirect('index.php');



    }

    public function trash(){

        $arrData  = array('true1'=>'1');
        $sql = 'UPDATE birthday  SET is_delete =true where id ='.$this->id;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Trashed Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Trashed Successfully!</h3></div>");


        Utility::redirect('index.php');


    }// end of trash()
       public function indexPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from birthday WHERE is_delete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byBirthday']) )  $sql = "SELECT * FROM `birthday` WHERE `is_delete` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `birthday` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byBirthday']) ) $sql = "SELECT * FROM `birthday` WHERE `is_delete` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byBirthday']) )  $sql = "SELECT * FROM `birthday` WHERE `is_delete` ='No' AND `birthday` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `birthday` WHERE `is_delete` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->birthday);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->birthday);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords






}