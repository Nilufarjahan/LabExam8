<?php
namespace App\City;
use App\Model\database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class City extends DB
{
    public $id="";
    public $name="";
    public $city="";
    public $country="";
    public function __construct(){

        parent::__construct();
    }
    public function setData($data=NULL){
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];

        }

        if (array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if (array_key_exists('country',$data))
        {
            $this->country=$data['country'];
        }
        if (array_key_exists('city',$data))
        {
            $this->city=$data['city'];
        }


    }
    public function store(){

        $arrData  = array($this->name,$this->country,$this->city);

        $sql = "INSERT INTO city( name,country,city) VALUES ( ?,?,?)";

        $STH = $this->DBH->prepare($sql);


        $result=$STH->execute($arrData);
        var_dump($result);
        if($result)
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] ,[ Country: $this->country ], [ City: $this->city ] <br> Data Has Been Inserted Successfully!</h5></div>");
        else
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] ,[ Country: $this->country ], [ City: $this->city ] <br> Data Has not Been Inserted Successfully!</h5></div>");
        Utility::redirect('create.php');

    }

    public function index($mode="ASSOC"){
        $mode=strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from city WHERE is_delete=0');


        if($mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }

    public function view($fetchMode="ASSOC"){

        $STH = $this->DBH->query('SELECT * from city where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of view()
    public function update(){

        $arrData  = array($this->name,$this->country,$this->city);

        $sql = 'UPDATE city  SET name  = ? , country  = ? , city = ? where id ='.$this->id;

        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('index.php');


    }// end of update()
    public function delete(){
        $sql='DELETE FROM city WHERE id = '.$this->id;
        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");

        Utility::redirect('index.php');



    }
    public function trash(){

        $arrData  = array('true1'=>'1');
        $sql = 'UPDATE city  SET is_delete=true where id ='.$this->id;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Trashed Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Trashed Successfully!</h3></div>");


        Utility::redirect('index.php');


    }// end of trash()
    public function recover(){

        $sql = "Update city SET is_delete='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('trashed.php');

    }// end of recover();
    public function indexPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from city WHERE is_delete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `city` WHERE `is_delete` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `city` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `city` WHERE `is_delete` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `city` WHERE `is_delete` ='No' AND `city` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `city` WHERE `is_delete` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->city);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->city);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords

    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from city where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();




}