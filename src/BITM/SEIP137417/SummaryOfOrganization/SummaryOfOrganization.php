<?php
namespace App\SummaryOfOrganization;
use App\Model\database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class SummaryOfOrganization extends DB
{
    public $id="";
    public $org_name="";
    public $org_summary="";

    public function __construct(){

        parent::__construct();
    }
    public function setData($data=NULL){
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];

        }

        if (array_key_exists('org_name',$data))
        {
            $this->org_name=$data['org_name'];
        }

        if (array_key_exists('org_summary',$data))
        {
            $this->org_summary=$data['org_summary'];
        }


    }
    public function store(){

        $arrData  = array($this->org_name,$this->org_summary);

        $sql = "INSERT INTO organization_of_summary ( org_name,org_summary) VALUES ( ?, ?)";

        $STH = $this->DBH->prepare($sql);


        $result=$STH->execute($arrData);
        var_dump($result);
        if($result)
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->org_name ] , [Organization Summary: $this->org_summary ] <br> Data Has Been Inserted Successfully!</h5></div>");
        else
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->org_name ] , [ Organization Summary: $this->org_summary ] <br> Data Has not Been Inserted Successfully!</h5></div>");
        Utility::redirect('create.php');

    }

    public function index($mode="ASSOC"){
        $mode=strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from organization_of_summary WHERE is_delete=0');


        if($mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }

    public function view($fetchMode="ASSOC"){

        $STH = $this->DBH->query('SELECT * from organization_of_summary where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of view()
    public function update(){

        $arrData  = array($this->org_name,$this->org_summary);

        $sql = 'UPDATE organization_of_summary SET org_name  = ?   , org_summary = ? where id ='.$this->id;

        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('index.php');


    }// end of update()
    public function delete(){
        $sql='DELETE FROM organization_of_summary WHERE id = '.$this->id;
        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");

        Utility::redirect('index.php');



    } 
    public function trash(){

        $arrData  = array('true1'=>'1');
        $sql = 'UPDATE organization_of_summary  SET is_delete=true where id ='.$this->id;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Trashed Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Trashed Successfully!</h3></div>");


        Utility::redirect('index.php');


    }// end of trash()
    public function recover(){

        $sql = "Update organization_of_summary SET is_delete='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('trashed.php');

    }// end of recover();
    public function indexPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from organization_of_summary  WHERE is_delete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `organization_of_summary` WHERE `is_delete` ='No' AND (`org_name` LIKE '%".$requestArray['search']."%' OR `org_summary` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `organization_of_summary` WHERE `is_delete` ='No' AND `org_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `organization_of_summary` WHERE `is_delete` ='No' AND `org_summary` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `organization_of_summary` WHERE `is_delete` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->org_name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->org_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->org_summary);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->org_summary);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords

    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from name where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();



}