<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Atomic Project</title>

<link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../../../resource/bootstrap/css/datepicker3.css" rel="stylesheet">
<link href="../../../resource/bootstrap/css/styles.css" rel="stylesheet">
<link rel="stylesheet" href="../../../resource/bootstrap/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="../../../resource/bootstrap/css/font-icons/font-awesome/css/font-awesome.min.css">
<script src="../../../resource/bootstrap/js/lumino.glyphs.js"></script>
    <script>
        $(document).ready(function(){
            $("#msg").delay(2500).fadeOut("slow");
        });
    </script>



</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Atomic </span>Project</a>
				
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		
		<ul class="nav menu">
		<li class="has-sub">
            <a href="#">
                <i class="fa fa-book"></i>
                <span class="title">Book Title</span>
            </a>
            <ul>
                <li>
                    <a href="../BookTitle/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../BookTitle/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="has-sub">
            <a href="#">
                <i class="fa fa-birthday-cake"></i>
                <span class="title">Birth Day</span>
            </a>
            <ul>
                <li>
                    <a href="../Birthday/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../Birthday/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-envelope"></i>
                <span class="title">Email Addess</span>
            </a>
            <ul>
                <li>
                    <a href="../Email/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../Email/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-male"></i>
                <span class="title">Gender</span>
            </a>
            <ul>
                <li>
                    <a href="../Gender/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../Gender/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-camera"></i>
                <span class="title">Profile Picture</span>
            </a>
            <ul>
                <li>
                    <a href="../ProfilePicture/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../ProfilePicture/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-tree"></i>
                <span class="title">City Name</span>
            </a>
            <ul>
                <li>
                    <a href="../City/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../City/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-tree"></i>
                <span class="title">Hobbies</span>
            </a>
            <ul>
                <li>
                    <a href="../Hobbies/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../Hobbies/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-tree"></i>
                <span class="title">Summary Of Organization</span>
            </a>
            <ul>
                <li>
                    <a href="../SummaryOfOrganization/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../SummaryOfOrganization/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
			
					</ul>

	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Birthday Form </div>
					<div class="panel-body">
						<div class="col-md-6">
							
							
    <form class="form-horizontal" action="store.php" method="post">
        <h2>Add Birthday</h2>
        <div class="form-group">
            <label class="control-label col-sm-2" for="bookname">Name:</label>
            <div class="col-sm-10">
                <input type="text" name= "name" class="form-control" id="book" placeholder="Enter  Name" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="authorname">Birthday:</label>
            <div class="col-sm-10">
                <input type="date" name="birthday" class="form-control" id="author" placeholder="Enter Birthday" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label><input type="checkbox"> Remember me</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit"  class="btn btn-success">Save</button>
               
            </div>
        </div>
    </form>
</div>

								
									
								</div>
								
								
								
							</div>
							
						
					</div>
				</div>
			</div><!-- /.col-->
		
	<script src="../../../resource/bootstrap/js/jquery-1.11.1.min.js"></script>
	<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
	<script src="../../../resource/bootstrap/js/chart.min.js"></script>
	<script src="../../../resource/bootstrap/js/chart-data.js"></script>
	<script src="../../../resource/bootstrap/js/easypiechart.js"></script>
	<script src="../../../resource/bootstrap/js/easypiechart-data.js"></script>
	<script src="../../../resource/bootstrap/js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
