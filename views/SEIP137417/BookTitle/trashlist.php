<?php
require_once ("../../../vendor/autoload.php");
session_start();
use App\BookTitle\BookTitle;
use App\Message\Message;
use App\Utility\Utility;

$objBookTitle = new BookTitle();
$allData = $objBookTitle->trashList();
$recordSet = $objBookTitle->trashList("OBJ");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Atomic Project By Mahmud Sabuj" />
    <meta name="author" content="Mahmud Sabuj" />

    <link rel="icon" href="../../../resource/img/favicon.ico">

    <title>Book Title List View | Atomic Project</title>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resource/bootstrap/css/jquery-ui.css">
    <script src="../../../resource/bootstrap/js/jquery-1.12.4.js"></script>
    <script src="../../../resource/bootstrap/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->


    <link rel="stylesheet" href="../../../resource/bootstrap/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/font-icons/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/core.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/theme.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/forms.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/custom.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/sabuj.css">


    <script src="../../../../assets/js/jquery-1.11.3.min.js"></script>
    <script src="../../../../assets/js/sabuj/me.js"></script>

    <!-- Imported scripts on this page -->
    <script src="../../../../assets/js/datatables/datatables.js"></script>
    <script src="../../../../assets/js/select2/select2.min.js"></script>
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="page-body">



<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <div class="sidebar-menu">

        <div class="sidebar-menu-inner">

            <header class="logo-env">

                <!-- logo -->
                <div class="logo">
                    <a href="../../../../index.php">
                        <h1>Home</h1>
                    </a>
                </div>

                <!-- logo collapse icon -->
                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>


                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>


            <ul id="main-menu" class="main-menu">
                <li class="opened has-sub">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span class="title">Book Title</span>
                    </a>
                    <ul>
                        <li>
                            <a href="create.php">
                                <span class="title">Add New</span>
                            </a>
                        </li>
                        <li>
                            <a href="index.php">
                                <span class="title">View All</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="has-sub">
                    <a href="#">
                        <i class="fa fa-birthday-cake"></i>
                        <span class="title">Birth Day</span>
                    </a>
                    <ul>
                        <li>
                            <a href="../Birthday/create.php">
                                <span class="title">Add New</span>
                            </a>
                        </li>
                        <li>
                            <a href="../Birthday/index.php">
                                <span class="title">View List</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="#">
                        <i class="fa fa-envelope"></i>
                        <span class="title">Email Addess</span>
                    </a>
                    <ul>
                        <li>
                            <a href="../Email/create.php">
                                <span class="title">Add New</span>
                            </a>
                        </li>
                        <li>
                            <a href="../Email/index.php">
                                <span class="title">View List</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="#">
                        <i class="fa fa-male"></i>
                        <span class="title">Gender</span>
                    </a>
                    <ul>
                        <li>
                            <a href="../Gender/create.php">
                                <span class="title">Add New</span>
                            </a>
                        </li>
                        <li>
                            <a href="../Gender/index.php">
                                <span class="title">View List</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="#">
                        <i class="fa fa-camera"></i>
                        <span class="title">Profile Picture</span>
                    </a>
                    <ul>
                        <li>
                            <a href="../ProfilePicture/create.php">
                                <span class="title">Add New</span>
                            </a>
                        </li>
                        <li>
                            <a href="../ProfilePicture/index.php">
                                <span class="title">View List</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="#">
                        <i class="fa fa-tree"></i>
                        <span class="title">City Name</span>
                    </a>
                    <ul>
                        <li>
                            <a href="../City/create.php">
                                <span class="title">Add New</span>
                            </a>
                        </li>
                        <li>
                            <a href="../City/index.php">
                                <span class="title">View List</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="#">
                        <i class="fa fa-play"></i>
                        <span class="title">Hobbies</span>
                    </a>
                    <ul>
                        <li>
                            <a href="../Hobbies/create.php">
                                <span class="title">Add New</span>
                            </a>
                        </li>
                        <li>
                            <a href="../Hobbies/index.php">
                                <span class="title">View List</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="#">
                        <i class="fa fa-sitemap"></i>
                        <span class="title">Organization Summary</span>
                    </a>
                    <ul>
                        <li>
                            <a href="../SummaryOfOrganization/create.php">
                                <span class="title">Add New</span>
                            </a>
                        </li>
                        <li>
                            <a href="../SummaryOfOrganization/index.php">
                                <span class="title">View List</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

        </div>

    </div>

    <div class="main-content">

        <ol class="breadcrumb bc-3" >
            <li>
                <a href="../../../../index.php"><i class="fa fa-home"></i>Home</a>
            </li>
            <li>
                <a href="index.php">Book Title</a>
            </li>
            <li class="active">
                <strong>View List</strong>
            </li>
        </ol>

        <div class="alert alert-success">
            <?php
            if(!isset( $_SESSION));
            echo Message::message();
            ?>
        </div>

        <table>
            <tr>
                <td width="500">
                    <h2>Trash List of Book Titles</h2>
                </td>

            </tr>
        </table>


    <form name="form" id="form" method="post" action="selecteditem.php" onSubmit="return validate();">
        <table class="table table-bordered datatable" id="table-3">
            <thead>
            <tr class="replace-inputs">
                <th class="col-sm-1">Select</th>
                <th class="col-sm-1">User ID</th>
                <th class="col-sm-2">Book Title</th>
                <th class="col-sm-2">Author Name</th>

                <th class="col-sm-4">Action</th>

            </tr>
            </thead>
            <tbody>

            <?php foreach($recordSet as $data) {?>
            <tr class="odd gradeX">
                <td><input name="checkbox[]" type="checkbox" id="checkbox[]" value="<?php echo $data->id; ?>"></td>
                <td><?php echo $data->id?></td>
                <td><?php echo $data->book_title?></td>
                <td><?php echo $data->author_name?></td>



                <td>
                    <a href="edits.php?id=<?php echo $data->id?>" class="btn btn-success btn-sm btn-icon icon-left" role="button">
                        <i class="entypo-pencil"></i>
                        Edit
                    </a>

                    <a href="trash.php?id=<?php echo $data->id?>" class="btn btn-orange btn-sm btn-icon icon-left">
                        <i class="entypo-trash"></i>
                        Trash
                    </a>
                    <a href="delete.php?id=<?php echo $data->id?>" onclick="return checkDelete()" class="btn btn-danger btn-sm btn-icon icon-left" role="button">
                        <i class="entypo-cancel"></i>
                        Delete
                    </a>
                    <a href="javascript:;" onclick="jQuery('').modal('show');" class="btn btn-info btn-sm btn-icon icon-left">
                        <i class="entypo-rocket"></i>
                        Mail a friend
                    </a>
                </td>
                <?php }?>
            </tr>
            </tbody>

            <tfoot>
            <tr>
                <th class="col-sm-1">Select</th>
                <th class="col-sm-1">ID</th>
                <th class="col-sm-2">Book Name</th>
                <th class="col-sm-2">Author Name</th>
                 <th class="col-sm-4">Action</th>
            </tr>
            </tfoot>
        </table>

        <table class="table">
            <tr>

                <td>
                    <input class="btn btn-danger btn-lg" name="delete" type="submit" id="delete" value="Parmanent Delete">

                </td>
                <td>
                    <input class="btn btn-warning btn-lg" name="recovery" type="submit" id="recovery" value="Restore Selected" />
                </td>

            </tr>
        </table>
    </form>

        <script language="JavaScript" type="text/javascript">
            function checkDelete() {
                return confirm ('Are yo sure want to delete this item?');
            }
            
        </script>



    </div>
</div>


<!-- Modal 1 (add new from index.php)-->
    <div class="modal fade" id="modal-1">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="well well-sm">
                        <h4>Please enter your book details.</h4>
                    </div>
                </div>

                <div class="modal-body">
                    <form id="rootwizard-2" method="post" action="storeindex.php" class="form-wizard validate">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab2-1">

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="full_name">Book Name</label>
                                            <input class="form-control" name="book_name" id="book_name" data-validate="required" placeholder="Book Name" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="birthdate">Author Name</label>
                                            <input class="form-control" name="author_name" id="author_name" data-validate="required" placeholder="Author Name" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="address_line_2">Book Category</label>
                                            <input class="form-control" name="category" id="book_category" data-validate="required" placeholder="Category" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="address_line_2">Price (TK)</label>
                                            <input class="form-control" name="price" id="book_price" data-validate="required" placeholder="Enter book price here" />
                                        </div>
                                    </div>

                                </div>



                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success"><i class="fa fa-bookmark"></i> Save </button>
                                <a type="" class="btn btn-info"><i class="fa fa-clock-o" aria-hidden="true"></i> Reset </a>
                                <button type="button" class="btn btn-orange pull-right" data-dismiss="modal"><i class="fa fa-arrow-down"></i> Close </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<!-- Modal 2 (delete)-->
    <div class="modal fade" id="modal-2">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete Single Item</h4>
                </div>

                <div class="modal-body">
                    Hey! Are you sure want to delete this item permanently?
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>

                    <a href="" class="btn btn-danger btn-sm btn-icon icon-left">
                        <i class="entypo-cancel"></i>
                        YES
                    </a>

                </div>
            </div>
        </div>
    </div>


<!-- Imported styles on this page -->
<link rel="stylesheet" href="../../../../assets/js/datatables/datatables.css">
<link rel="stylesheet" href="../../../../assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="../../../../assets/js/select2/select2.css">

<!-- Bottom scripts (common) -->
<script src="../../../../assets/js/gsap/TweenMax.min.js"></script>
<script src="../../../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="../../../../assets/js/bootstrap.js"></script>
<script src="../../../../assets/js/joinable.js"></script>
<script src="../../../../assets/js/resizeable.js"></script>
<script src="../../../../assets/js/atomic-api.js"></script>





<!-- JavaScripts initializations and stuff -->
<script src="../../../../assets/js/custom.js"></script>




</body>
</html>


<script language="javascript">
    function validate()
    {
        var chks = document.getElementsByName('checkbox[]');
        var hasChecked = false;
        for (var i = 0; i < chks.length; i++)
        {
            if (chks[i].checked)
            {
                hasChecked = true;
                break;
            }
        }
        if (hasChecked == false)
        {
            alert("Please select at least one.");
            return false;
        }
        return true;
    }
</script>
