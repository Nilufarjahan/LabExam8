<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Email\Email;

if(!isset( $_SESSION)) session_start();
echo Message::message();
$emailObject=new Email();
$emailObject->setData($_GET);
$singleItem=$emailObject->view("obj");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <title>Atomic Project Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#msg").delay(2500).fadeOut("slow");
        });
    </script>
</head>
<body>

<div class="container" style="height:400px;background-color: cadetblue">
    <form class="form-horizontal" action="update.php" method="post">
        <input hidden name="id" value="<?php echo $singleItem->id; ?>">
        <h2>Edit Email </h2>
        <div class="form-group">
            <label class="control-label col-sm-2" for="bookname">Name:</label>
            <div class="col-sm-10">
                <input type="text" name= "name" class="form-control" id="name" value="<?php echo $singleItem->name?> " required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="authorname">Email:</label>
            <div class="col-sm-10">
                <input type="email" name="email" class="form-control" id="email" value="<?php echo $singleItem->email?> " required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label><input type="checkbox"> Remember me</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit"  class="btn btn-success">Save</button>

            </div>
        </div>
    </form>
</div>
</body>
</html>

