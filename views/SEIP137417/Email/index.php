<?php
require_once("../../../vendor/autoload.php");
use App\Email\Email;
use App\Message\Message;
use App\Utility\Utility;
$emailObject = new Email();
$allData = $emailObject->index("obj");



################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$allData =  $emailObject->search($_REQUEST);
$availableKeywords=$emailObject->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################





######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $emailObject->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################



################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $emailObject->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################


?>






<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Atomic Project</title>

    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../resource/bootstrap/css/datepicker3.css" rel="stylesheet">
    <link href="../../../resource/bootstrap/css/styles.css" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/font-icons/font-awesome/css/font-awesome.min.css">
    <script src="../../../resource/bootstrap/js/lumino.glyphs.js"></script>
    <script>
        $(document).ready(function(){
            $("#msg").delay(2500).fadeOut("slow");
        });
    </script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resource/bootstrap/css/jquery-ui.css">
    <script src="../../../resource/bootstrap/js/jquery-1.12.4.js"></script>
    <script src="../../../resource/bootstrap/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->


</head>


<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>Atomic </span>Project</a>

        </div>

    </div><!-- /.container-fluid -->
</nav>
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

    <ul class="nav menu">
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-book"></i>
                <span class="title">Book Title</span>
            </a>
            <ul>
                <li>
                    <a href="../BookTitle/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../BookTitle/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="has-sub">
            <a href="#">
                <i class="fa fa-birthday-cake"></i>
                <span class="title">Birth Day</span>
            </a>
            <ul>
                <li>
                    <a href="../Birthday/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../Birthday/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-envelope"></i>
                <span class="title">Email Addess</span>
            </a>
            <ul>
                <li>
                    <a href="../Email/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../Email/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-male"></i>
                <span class="title">Gender</span>
            </a>
            <ul>
                <li>
                    <a href="../Gender/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../Gender/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-camera"></i>
                <span class="title">Profile Picture</span>
            </a>
            <ul>
                <li>
                    <a href="../ProfilePicture/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../ProfilePicture/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-tree"></i>
                <span class="title">City Name</span>
            </a>
            <ul>
                <li>
                    <a href="../City/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../City/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-tree"></i>
                <span class="title">Hobbies</span>
            </a>
            <ul>
                <li>
                    <a href="../Hobbies/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../Hobbies/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="#">
                <i class="fa fa-tree"></i>
                <span class="title">Summary Of Organization</span>
            </a>
            <ul>
                <li>
                    <a href="../SummaryOfOrganization/create.php">
                        <span class="title">Add New</span>
                    </a>
                </li>
                <li>
                    <a href="../SummaryOfOrganization/index.php">
                        <span class="title">View List</span>
                    </a>
                </li>
            </ul>
        </li>

    </ul>

</div><!--/.sidebar-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li class="active">Icons</li>
        </ol>
    </div><!--/.row-->


    <table>
        <tr>
            <td width="450">
                <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
                <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
                <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>
            </td>
            <td width="150">


                <!-- required for search, block 4 of 5 start -->


                <form id="searchForm" action="index.php"  method="get">
                    <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
                    <input type="checkbox"  name="byName"   checked  >By Name
                    <input type="checkbox"  name="byEmail"   checked  >By Email
                    <input hidden type="submit" class="btn-primary" value="search">
                </form>

                <!-- required for search, block 4 of 5 end -->


            </td>
        </tr>
    </table>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Email Table </div>
                <div class="panel-body">
                    <div class="col-md-12">

                        <h2>Email Information</h2>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="text-align: center">Serial</th>
                                <th style="text-align: center">Id</th>
                                <th style="text-align: center">Name</th>
                                <th style="text-align: center">Email</th>

                                <th style="text-align: center">Option</th>


                            </tr>
                            </thead>
                            <tbody>
                            <tr>

                                <?php
                                foreach($allData as $oneData)
                                {
                                ?>

                            <tr>
                                <td><?php echo ++$serial ?> </td>
                                <td><?php echo $oneData->id ?> </td>
                                <td><?php echo $oneData->name ?></td>
                                <td><?php echo $oneData->email?></td>

                                <?php  echo "<td> <a href=\"edits.php?id=$oneData->id\"class=\"btn btn-info\" role=\"button\">Edit</a>  ";?>
                                <?php  echo " <a href=\"delete.php?id=$oneData->id\"class=\"btn btn-danger\" role=\"button\">Delete</a> ";?>
                                <?php  echo " <a href=\"recover.php?id=$oneData->id\"class=\"btn btn-info\" role=\"button\">Recover</a> ";?>
                                <?php  echo " <a href=\"trash.php?id=$oneData->id\"class=\"btn btn-danger\" role=\"button\">Trash</a> </td> ";?>
                            </tr>
                            <?php  }
                            ?>

                            </tbody>
                        </table>


                    </div>



                </div>



            </div>


        </div>
    </div>
</div><!-- /.col-->







<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="center" class="container">
    <ul class="pagination">

        <?php

        $pageMinusOne  = $page-1;
        $pagePlusOne  = $page+1;
        if($page>$pages) Utility::redirect("index.php?Page=$pages");

        if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->



</body>
</html>




<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->
