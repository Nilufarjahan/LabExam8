<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Gender\Gender;

if(!isset( $_SESSION)) session_start();
echo Message::message();
$genderObject=new Gender();
$genderObject->setData($_GET);
$singleItem=$genderObject->view("obj");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <title>Atomic Project Gender</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#msg").delay(2500).fadeOut("slow");
        });
    </script>
</head>
<body>

<div class="container" style="height:400px;background-color: cadetblue">
    <form class="form-horizontal" action="update.php" method="post">
        <input hidden name="id" value="<?php echo $singleItem->id; ?>">
        <h2>Edit Gender </h2>
        <div class="form-group">
            <label class="control-label col-sm-2" for="bookname"> Name:</label>
            <div class="col-sm-10">
                <input type="text" name= "name" class="form-control" id="book" value="<?php echo $singleItem->name?> " required>
            </div>
        </div>

 <div class="form-group">
            <label class="control-label col-sm-2" for="authorname">Gender:</label>
            <div class="col-sm-10">
                <ul class="icheck-list">

                                <input class="icheck-2" type="radio" id="minimal-radio-1-2" name="gender" value="Male" <?php if(strpos($singleItem->gender,"Male")!==false)echo 'checked="checked"';?>>
                                <label for="minimal-radio-1-2">Male</label>


                                <input tabindex="8" class="icheck-2" type="radio" id="minimal-radio-2-2" name="gender" value="Female" <?php if(strpos($singleItem->gender,"Female")!==false)echo 'checked="checked"';?>>
                                <label for="minimal-radio-2-2">Female</label>


                                <input tabindex="8" class="icheck-2" type="radio" id="minimal-radio-2-2" name="gender" value="Other" <?php if(strpos($singleItem->gender,"Other")!==false)echo 'checked="checked"';?>>
                                <label for="minimal-radio-2-2">Other</label>
                         
                        </ul>
   </div>
</div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label><input type="checkbox"> Remember me</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit"  class="btn btn-success">Save</button>

            </div>
        </div>
    </form>
</div>
</body>
</html>

